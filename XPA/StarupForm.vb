﻿Imports System.Deployment.Application ' SetAddRemoveProgramsIcon()
Imports Microsoft.Win32 '               SetAddRemoveProgramsIcon()
Imports System.IO '                     SetAddRemoveProgramsIcon(), StreamWriter()

Public Class StarupForm

    Public Shared Function SetAddRemoveProgramsIcon() As Boolean
        Const iName As String = "LogoFinal.ico"
        Const aName As String = "XPA"
        ' the "Assembly name" in Properties -> Application if "Product name" is empty, or
        ' the "Product name" in Project Properties -> Publish -> Options...
        Try
            'string iconSourcePath = Path.Combine(Application.StartupPath, "Resources\\", iName);
            Dim iconSourcePath As String = Path.Combine(Application.StartupPath, iName)
            If Not ApplicationDeployment.IsNetworkDeployed Then Return False
            If Not ApplicationDeployment.CurrentDeployment.IsFirstRun Then Return False
            If Not File.Exists(iconSourcePath) Then Return False
            Dim myUninstallKey As RegistryKey =
                Registry.CurrentUser.OpenSubKey _
                ("Software\Microsoft\Windows\CurrentVersion\Uninstall")
            Dim mySubKeyNames() As String = myUninstallKey.GetSubKeyNames()
            For i As Integer = 0 To mySubKeyNames.Length - 1
                Dim myKey As RegistryKey = myUninstallKey.OpenSubKey(mySubKeyNames(i), True)
                Dim myValue As Object = myKey.GetValue("DisplayName")
                If myValue <> Nothing And myValue.ToString() = aName Then
                    myKey.SetValue("DisplayIcon", iconSourcePath)
                    Return True
                End If
            Next
        Catch
            Return False
        End Try
        Return False
    End Function

    Private Sub StarupForm_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        SetAddRemoveProgramsIcon()
    End Sub

End Class
